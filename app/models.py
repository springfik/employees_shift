from sqlalchemy import Column, Integer, String
from . import database
from sqlalchemy.dialects.sqlite import DATE

class Employees(database.Base):
  __tablename__ = 'employees'
	
  id = Column(Integer, primary_key=True, nullable=False)
  username = Column(String, nullable=False, unique=True)  # Логин пользователя для входа
  password = Column(String, nullable=False)
  salary = Column(Integer, nullable=False) 
  promotion_date = Column(DATE(), nullable=True)
