from jose import JWTError, jwt
from fastapi import Depends, status, HTTPException
from passlib.context import CryptContext
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from datetime import datetime, timedelta
from . import database, models, schemes

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth")

SECRET_KEY = "superdupersecret"
TOKEN_EXPIRE_MINUTE = 10
ALGORITHM = "HS256"

def hash_password(password: str):
  return pwd_context.hash(password)

def verify_password(plain_password: str, hashed_password: str):
  return pwd_context.verify(plain_password, hashed_password)

def create_access_token(data: dict):
  to_encode = data.copy()
  expire = datetime.utcnow() + timedelta(TOKEN_EXPIRE_MINUTE)
  to_encode.update({"exp": expire})
  jwt_token = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
  return jwt_token
	
def get_employeer(token: str = Depends(oauth2_scheme), db: Session = Depends(database.get_db)):
  credentials_exception = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                        detail="Не удалось проверить учетные данные",
                                        headers={"WWW-Authenticate": "Bearer"})
																				
  try:
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    id: str = payload.get("user_id")
    if id is None:
      raise credentials_exception
    token_data = schemes.TokenData(id=id)
		
  except JWTError:
    raise credentials_exception
		
  employeer = db.query(models.Employees).filter(models.Employees.id == token_data.id).first()
	
  return employeer
