from fastapi import FastAPI, Depends, HTTPException, status
from sqlalchemy.orm import Session
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from . import utils, schemes, models, database

models.database.Base.metadata.create_all(bind=database.engine)

app = FastAPI()

@app.post("/emps", response_model=schemes.UserResponseValid)
def create_emp(emp: schemes.Employee, db: Session = Depends(database.get_db)):
  hashed_password = utils.hash_password(emp.password)
  emp.password = hashed_password
  new_user = models.Employees(**emp.dict())
  db.add(new_user)
  try:
    db.commit()
  except BaseException:
    db.rollback()
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Такой пользователь уже существует")

  db.refresh(new_user)
  return new_user

@app.post("/auth", response_model=schemes.Token)
def auth(emp_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(database.get_db)):
  employeer = db.query(models.Employees).filter(models.Employees.username == emp_data.username).first()

  if not employeer:
    raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Неверные учетные данные")
	
  if not utils.verify_password(emp_data.password, employeer.password):
    raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Неверные учетные данные")
	
  access_token = utils.create_access_token(data={"user_id": employeer.id})

  return {"access_token": access_token, "token_type": "Bearer"} 
	
@app.get("/salary")
def get_salary(db: Session = Depends(database.get_db), current_user: str = Depends(utils.get_employeer)):
  salary = db.query(models.Employees).filter(models.Employees.id == current_user.id).first()
	
  return salary
