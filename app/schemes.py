from pydantic import BaseModel
from typing import Optional
import datetime

class Employee(BaseModel):
  username: str
  password: str
  salary: int = 0
  promotion_date: datetime.date = None
	
class Token(BaseModel):
  access_token: str
  token_type: str
	
class TokenData(BaseModel):
  id: Optional[int] = None
	
class UserResponseValid(BaseModel):  # Валидация ответа пользователю на "создание нового пользователя"
  id: int
  username: str
  salary: int
  promotion_date: datetime.date or None = None

  class Config:
    orm_mode = True
